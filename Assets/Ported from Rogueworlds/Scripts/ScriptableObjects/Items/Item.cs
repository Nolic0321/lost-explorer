﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class Item : ScriptableObject
    {
        public string itemName;
        public string Description;
        public Sprite Icon = null;

        public virtual void Init(string n, string d, Sprite i)
        {
            itemName = n;
            Description = d;
            Icon = i;
        }

        public virtual void Use()
        {
            Debug.Log("Using " + itemName);
        }

        public virtual void OnDrop()
        {
            Debug.Log("Dropping " + itemName);
        }
    }
}