﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class Equipment : Item
    {
        public EquipmentSlot EquipSlot;
        public GameObject EquipablePrefab;

        public virtual void Init(string n, string d, Sprite i, EquipmentSlot e, GameObject ep)
        {
            Init(n, d, i);
            EquipSlot = e;
            EquipablePrefab = ep;
        }

        public override void Init(string n, string d, Sprite i)
        {
            base.Init(n, d, i);
        }

        public override void Use()
        {
            base.Use();
        }
    }

    public enum EquipmentSlot { Head, Chest, MainHand, OffHand }
}