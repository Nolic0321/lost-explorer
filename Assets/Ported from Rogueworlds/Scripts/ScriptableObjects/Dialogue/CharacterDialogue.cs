﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialogue/Character Dialogue")]
public class CharacterDialogue : Dialogue {
    public Sprite characterPortrait;
    public string characterName;
}
