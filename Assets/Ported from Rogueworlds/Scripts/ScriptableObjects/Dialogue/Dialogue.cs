﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Dialogue : ScriptableObject {
    [TextArea(3,10)]
    public string[] sentences;
    }
