﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    [CreateAssetMenu(menuName = "Abilities/DroidProjectileAbility")]
    public class DroidProjectileAbility : Ability
    {
        public int damage = 1;
        public int speed = 1;
        public float projectileAliveTime = 3f;
        public GameObject prefab;

        private Droid droid;

        public override void Initialize(GameObject obj)
        {
            droid = obj.GetComponent<Droid>();
            droid.Initialize();
        }

        public override void TriggerAbility(Droid droid)
        {
            GameObject projectile = Instantiate(prefab, droid.transform.position, Quaternion.identity);
            projectile.GetComponent<Rigidbody2D>().velocity =
                Camera.main.ScreenToWorldPoint(Input.mousePosition) - droid.transform.position * speed;
            //NetworkServer.Spawn(projectile);
        }

        private IEnumerator DestroyAfterSeconds(GameObject obj, int duration)
        {
            yield return new WaitForSecondsRealtime(duration);
            NetworkServer.Destroy(obj);
        }
    }
}