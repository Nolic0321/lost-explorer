﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public abstract class Ability : ScriptableObject
    {
        public string abilityName = "New Ability";
        public AudioClip sound;
        public float baseCooldown = 1f;

        public abstract void Initialize(GameObject obj);

        public abstract void TriggerAbility(Droid droid);
    }
}