﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    [CreateAssetMenu(fileName = "Droplist")]
    public class DropList : ScriptableObject
    {
        public Item[] List;
    }
}