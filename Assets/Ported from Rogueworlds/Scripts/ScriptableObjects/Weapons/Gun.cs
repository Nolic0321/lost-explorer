﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    [CreateAssetMenu(fileName = "Gun", menuName = "Weapons/Gun")]
    public class Gun : Weapon
    {
        public GameObject ProjectilePrefab;
    }
}