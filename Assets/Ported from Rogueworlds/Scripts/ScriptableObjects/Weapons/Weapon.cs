﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class Weapon : Equipment
    {
        
        public Stat Damage;
        public bool Onehanded = false;
        public bool Twohanded = false;

        public void Init(string n, string d, Sprite i, EquipmentSlot e, GameObject ep, Stat dg, bool h, bool hh)
        {
            Init(n, d, i, e, ep);
            Damage = dg;
            Onehanded = h;
            Twohanded = hh;
        }
    }
}