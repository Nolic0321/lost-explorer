﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    [CreateAssetMenu(fileName = "Sword", menuName = "Weapons/Sword")]
    public class Sword : Weapon
    {
    }
}