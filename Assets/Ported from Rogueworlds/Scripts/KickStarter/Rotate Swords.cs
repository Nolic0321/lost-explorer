﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSwords : MonoBehaviour {

    public float rotationMax = 0f;

    [Range(1,4)]
    public float randomTimeWaitMax = 2f;

    private Quaternion startRot;
	// Use this for initialization
	void Start () {
		startRot = transform.rotation;
	}
	
	IEnumerator WaitAndRotate()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1,randomTimeWaitMax));
            var i = 0;
            var rotateTo = Random.Range(0,rotationMax);
            Quaternion finalRotation = Quaternion.Euler(0,0,rotateTo);
            while(transform.rotation != finalRotation)
            {
                transform.rotation = Quaternion.Lerp(startRot,finalRotation,Time.fixedDeltaTime);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
