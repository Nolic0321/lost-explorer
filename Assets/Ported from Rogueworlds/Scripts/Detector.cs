﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{
    [SerializeField] private EnemyAI _ai;
    [SerializeField] private Attackable _myTargets;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (_myTargets.IsATarget(collision.tag))
        {
            //Do Something

        }
    }
}
