﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private Attackable _parent;
        [SerializeField] public int dmg = 10;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            CharacterStats targetStats = collision.gameObject.GetComponent<CharacterStats>();
            if (targetStats && _parent.IsATarget(collision.tag))
            {
                targetStats.TakeDamage(dmg);
                Destroy(gameObject);
            }
        }
    }
}