﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DevEditorCheck : MonoBehaviour {

    private void Awake() {
#if UNITY_EDITOR
        if (EditorApplication.isPlaying)
            return;

#endif
        GameObject.Destroy(gameObject);
    }

}