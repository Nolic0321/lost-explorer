﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAtMouseScript : MonoBehaviour {

    [Header("Rotate Transform")]
    public bool MoveTransform = false;
    [Range(0, 45)]
    public float degreeRotation = 25;

    private Quaternion startingRot;

    private void Awake() {
        startingRot = transform.rotation;
    }
    // Update is called once per frame
    private void Update() {
        if (MoveTransform) {
            transform.rotation = Quaternion.AngleAxis(Mathf.Clamp(AngleToMouse(transform.position), -degreeRotation, degreeRotation), Vector3.forward);
        }
    }

    public static float AngleToMouse(Vector3 origin) {
        Vector3 pos = Camera.main.WorldToScreenPoint(origin);
        Vector3 dir = Input.mousePosition - pos;
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }
}