﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    public class PrefabManager : MonoBehaviour
    {
        protected PrefabManager()
        {
        }

        // Assign the prefab in the inspector
        public GameObject[] EnemyPrefabs;

        private void Start()
        {
            SpawnEnemy();
        }

        public void SpawnEnemy(Vector2 pos)
        {
            GameObject obj = PhotonNetwork.InstantiateSceneObject(EnemyPrefabs[Random.Range(0, EnemyPrefabs.Length)].name, pos, Quaternion.identity);
            obj.GetComponent<EnemyStats>().prefabManager = this;
        }

        public void SpawnEnemy()
        {
            SpawnEnemy(transform.position);
        }
    }
}