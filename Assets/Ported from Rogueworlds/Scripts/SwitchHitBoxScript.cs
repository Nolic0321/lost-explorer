﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    public class SwitchHitBoxScript : MonoBehaviour
    {
        [SerializeField] private Attackable _parent;
        [SerializeField] private CharacterStats _stats;
        public bool IsAttacking = false;

        private void DisableComponent()
        {
            GetComponent<SwitchHitBoxScript>().enabled = false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!IsAttacking)
                return;
            CharacterStats targetStats = collision.gameObject.transform.root.GetComponent<CharacterStats>();
            if (targetStats && _parent.IsATarget(collision.tag))
            {
                if (transform.root.GetComponent<NetworkIdentity>().isLocalPlayer)
                    throw new NotImplementedException();
                else
                    _stats.Attack(targetStats);
                IsAttacking = false;
            }
        }
    }
}