﻿using PandaRooStudios.LostExplorer;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    public class ItemDrop : MonoBehaviour
    {
        private WaitForFixedUpdate _waitForFixedUpdate = new WaitForFixedUpdate();
        [SerializeField] private Item _item;
        [SerializeField] private float _bounceSize = .3f;
        [SerializeField] private float _suctionRange = 2f;
        [SerializeField] private float _followSpeed = 1f;
        [Range(.1f, 1)] [SerializeField] private float _bounceTime = 1f;
        [SerializeField] private DropList _dropList;
        private Coroutine _bounceRoutine;

        private Inventory _inventory;
        private Transform _localPlayer;
        private bool _goToPlayer = false;
        private bool _isBouncing = false;

        private Vector2 _startPos;

        // Use this for initialization
        private void Start()
        {
            _inventory = GameObject.Find("GameManager").GetComponent<Inventory>();
            _startPos = transform.position;
            _item = _dropList.List[Random.Range(0, _dropList.List.Length)];
            //_localPlayer =
            Init();
        }

        public void Init()
        {
            GetComponent<SpriteRenderer>().sprite = _item.Icon;
            StartBounceCoroutine();
        }

        private void StartBounceCoroutine()
        {
            if (!_isBouncing)
            {
                StartCoroutine("Bounce");
                _isBouncing = true;
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (Vector2.Distance(_localPlayer.position, transform.position) < _suctionRange)
            {
                if (_isBouncing)
                    StopCoroutine("Bounce");
                _isBouncing = false;
                _goToPlayer = true;
            }
            else if (_goToPlayer && Vector2.Distance(_localPlayer.position, transform.position) > _suctionRange * 2)
            {
                StartBounceCoroutine();
                _goToPlayer = false;
            }
        }

        private void FixedUpdate()
        {
            if (_goToPlayer)
                transform.position = Vector2.Lerp(transform.position, _localPlayer.position, _followSpeed * Time.fixedDeltaTime);
        }

        private IEnumerator Bounce()
        {
            float elapsedTime = 0f;
            while (true)
            {
                _startPos = transform.position;
                elapsedTime = 0f;
                while (elapsedTime < _bounceTime)
                {
                    transform.position = Vector2.Lerp(_startPos, _startPos + Vector2.up * _bounceSize, elapsedTime / _bounceTime);
                    yield return new WaitForFixedUpdate();
                    elapsedTime += Time.deltaTime;
                }
                _startPos = transform.position;
                elapsedTime = 0f;
                while (elapsedTime < _bounceTime)
                {
                    transform.position = Vector2.Lerp(_startPos, _startPos + Vector2.down * _bounceSize, elapsedTime / _bounceTime);
                    yield return new WaitForFixedUpdate();
                    elapsedTime += Time.deltaTime;
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject == _localPlayer.gameObject)
            {
                if (_inventory.Add(_item))
                    Destroy(gameObject);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _suctionRange);
        }
    }
}