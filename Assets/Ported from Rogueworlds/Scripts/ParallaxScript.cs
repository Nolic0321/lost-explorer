﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class ParallaxScript : MonoBehaviour
{
    public bool isScrolling, parallax;
    public float backGroundSize;
    public float parallaxSpeed;
    [Range(0, 100)] public int renderPercent = 50;
    public float foreGroundHeight;
    

    [SerializeField] private Sprite[] randomSprites;
    [SerializeField] private Sprite floorSprite;
    [SerializeField] private float endOfViewBuffer = 3f;
    private System.Random random;
    private Transform cameraTransform;
    private Transform[] layers;
    private float viewZone;
    private int leftIndex;
    private int rightIndex;
    private float lastCameraX;

    private void Start()
    {
        cameraTransform = Camera.main.transform;
        random = new Random();
        viewZone = Camera.main.orthographicSize * Camera.main.aspect - endOfViewBuffer;
        layers = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            layers[i] = transform.GetChild(i);
            if (floorSprite)
            {
                SpriteRenderer sprite = layers[i].GetOrAddComponent<SpriteRenderer>();
                sprite.sprite = floorSprite;
                RandomSpriteSpawn(layers[i]);
            }
        }

        leftIndex = 0;
        rightIndex = layers.Length - 1;

        lastCameraX = cameraTransform.position.x;
    }

    private void Update()
    {
        if (parallax)
        {
            float deltaX = cameraTransform.position.x - lastCameraX;
            transform.position += Vector3.right * (deltaX * parallaxSpeed);
        }
        lastCameraX = cameraTransform.position.x;
        if (isScrolling)
        {
            if (cameraTransform.position.x < (layers[leftIndex].transform.position.x + viewZone))
                ScrollLeft();
            if (cameraTransform.position.x > (layers[rightIndex].transform.position.x - viewZone))
                ScrollRight();
        }
    }

    void ScrollLeft()
    {
        int lastRight = rightIndex;
        layers[rightIndex].localPosition = Vector3.right * (layers[leftIndex].localPosition.x - backGroundSize);
        RandomSpriteSpawn(layers[rightIndex]);
        leftIndex = rightIndex;
        rightIndex--;
        if (rightIndex < 0)
            rightIndex = layers.Length - 1;
    }

    private void RandomSpriteSpawn(Transform transform)
    {
        if (transform.childCount > 0)
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        if (UnityEngine.Random.Range(0, 101) <= renderPercent && randomSprites.Length > 0)
        {
            SpriteRenderer transformRenderer = transform.GetComponent<SpriteRenderer>();
            GameObject sprite = new GameObject();
            SpriteRenderer renderer = sprite.AddComponent<SpriteRenderer>();
            renderer.sprite = randomSprites[random.Next(randomSprites.Length - 1)];
            renderer.color = transform.GetComponent<SpriteRenderer>().color;
            renderer.sortingOrder = transformRenderer.sortingOrder;
            renderer.sortingLayerName = transformRenderer.sortingLayerName;
            GameObject obj = Instantiate(sprite, transform);
            obj.transform.localPosition = new Vector3(0, foreGroundHeight, 0);
            Destroy(sprite.gameObject);
        }
    }

    void ScrollRight()
    {
        int lastleft = leftIndex;
        layers[leftIndex].localPosition = Vector3.right * (layers[rightIndex].localPosition.x + backGroundSize);
        RandomSpriteSpawn(layers[leftIndex]);
        rightIndex = leftIndex;
        leftIndex++;
        if (leftIndex == layers.Length)
            leftIndex = 0;
    }
}
