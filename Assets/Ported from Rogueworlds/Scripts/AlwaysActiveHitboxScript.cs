﻿using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    public class AlwaysActiveHitboxScript : MonoBehaviour
    {
        [SerializeField] private Attackable _parent;
        [SerializeField] internal CharacterStats _stats;

        private void DisableComponent()
        {
            GetComponent<AlwaysActiveHitboxScript>().enabled = false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            CharacterStats targetStats = collision.gameObject.GetComponent<CharacterStats>();
            if (targetStats && _parent.IsATarget(collision.tag))
            {
                _stats.Attack(targetStats);
            }
        }
    }
}