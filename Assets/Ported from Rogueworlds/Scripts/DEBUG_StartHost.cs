﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DEBUG_StartHost : MonoBehaviour {

	// Use this for initialization
	void Start () {
		NetworkManager.singleton.StartHost();
	}
}
