﻿// This file is auto-generated. Do not modify or move this file.

using System;
using UnityEngine;
using UnityEngine.Networking;

public class UNetChat : Chat
{
    private const short _chatMessage = 0321;
    private string _myGuid;

    public delegate void ChatSelected(bool selected);

    public ChatSelected chatSelectedCallback;



    private void Awake()
    {
        _myGuid = Guid.NewGuid().ToString();
    }

    private void Start()
    {
        if (NetworkServer.active)
        {
            NetworkServer.RegisterHandler(_chatMessage, ServerReceiveMessage);
        }

        NetworkManager.singleton.client.RegisterHandler(_chatMessage, ReceiveMessage);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            
            SendMessage(player, inputField.text);
            inputField.text = "";
            ChatDeselectedCallbackInvoke();

        }
    }

    public void ChatSelectedCallbackInvoke()
    {
        if(chatSelectedCallback != null)
            chatSelectedCallback.Invoke(true);
    }

    public void ChatDeselectedCallbackInvoke()
    {
        if(chatSelectedCallback != null)
            chatSelectedCallback.Invoke(false);
    }

    private void ReceiveMessage(NetworkMessage message)
    {
        PlayerMessage pMessage = message.ReadMessage<PlayerMessage>();
        string text = String.Format("{0}: {1}", pMessage.player, pMessage.message);

        AddMessage(text);
    }

    public override void SendMessage(string player, string message)
    {
        if(message.Equals(""))
            return;
        
        PlayerMessage myMessage = new PlayerMessage
        {
            player = player,
            message = message,
            messageId = _myGuid
        };

        NetworkManager.singleton.client.Send(_chatMessage, myMessage);
    }

    private void ServerReceiveMessage(NetworkMessage message)
    {
        PlayerMessage myMessage = message.ReadMessage<PlayerMessage>();
        NetworkServer.SendToAll(_chatMessage, myMessage);
    }

    private void OnDestroy()
    {
        if (NetworkManager.singleton)
        {
            if (NetworkServer.active)
                NetworkServer.UnregisterHandler(_chatMessage);

            NetworkManager.singleton.client.UnregisterHandler(_chatMessage);
        }
    }
}

public class PlayerMessage : MessageBase
{
    public string player;
    public string message;
    public string messageId;
}