﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    public class ProjectileDroid : Droid
    {
        [SerializeField] private int shootRefresh = 4;
        [SerializeField] private int projectileDuration = 3;
        [SerializeField] private int projectileSpeed = 6;
        [SerializeField] private int projectileDmg = 5;
        [SerializeField] private GameObject projectilePrefab;

        private void Start()
        {
            ActivateAbility();
        }

        protected override void ActivateAbility()
        {
            StartCoroutine(StartAbilityLoop());
            base.ActivateAbility();
        }

        private IEnumerator StartAbilityLoop()
        {
            while (true)
            {
                Vector2 aimToMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
                if (hasAuthority)
                    CmdShoot(aimToMouse);
                yield return new WaitForSecondsRealtime(Random.Range(1, shootRefresh));
            }
        }

        [Command]
        private void CmdShoot(Vector2 direction)
        {
            GameObject projectile = Instantiate(projectilePrefab, transform.position, Quaternion.FromToRotation(Vector2.right, direction));
            projectile.GetComponent<Rigidbody2D>().velocity = direction.normalized * projectileSpeed;
            projectile.GetComponent<Attackable>().EnemyTags = GetComponent<Attackable>().EnemyTags;
            projectile.GetComponent<Projectile>().dmg = projectileDmg;
            NetworkServer.Spawn(projectile);
            Destroy(projectile, projectileDuration);
        }
    }
}