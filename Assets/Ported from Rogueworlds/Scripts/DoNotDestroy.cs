﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNotDestroy : MonoBehaviour
{
    private static DoNotDestroy _doNotDestroyInstance;
	// Use this for initialization
    private void Awake () {

        if (_doNotDestroyInstance == null)
        {
            DontDestroyOnLoad(gameObject);
            _doNotDestroyInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
            
	}
}
