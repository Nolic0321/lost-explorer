﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _singlePlayerButton;
    [SerializeField] private Button _multiPlayerButton;
    [SerializeField] private Button _optionsButton;
    [SerializeField] private Button _quitButton;
    [SerializeField] private ConnectionManagerScript _manager;
    [SerializeField] private GameObject _bugReportingPrefab;

    private MenuSwapper _menuSwapper;

    private void Awake()
    {
    }

    // Use this for initialization
    private void Start()
    {
        _manager = GameObject.Find("NetworkManager").GetComponent<ConnectionManagerScript>();
        _menuSwapper = GetComponent<MenuSwapper>();
        ButtonSetup();
    }

    private void ButtonSetup()
    {
        _singlePlayerButton.onClick.AddListener(SinglePlayer);
        _multiPlayerButton.onClick.AddListener(Multiplayer);
        _optionsButton.onClick.AddListener(Options);
        _quitButton.onClick.AddListener(Quit);
    }

    private void SinglePlayer()
    {
        _manager.StartLan();
    }

    private void Multiplayer()
    {
        //GameObject.Find("GameManager").GetComponent<GameManager>().SinglePlayer = false;
        _menuSwapper.ToggleMultiplayerMenu();
    }

    private void Options()
    {
        //STub
    }

    private void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
}