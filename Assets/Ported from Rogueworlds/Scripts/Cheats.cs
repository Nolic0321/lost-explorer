﻿using PandaRooStudios.LostExplorer;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class Cheats
    {
        public Item Sword;
        public Inventory Inventory;

        public Cheats()
        {
        }

#if UNITY_EDITOR

        [MenuItem("Roguelands Magicite War/Cheats/AddPlasmaSword")]
        public static void AddPlasmaSword()
        {
            if (Application.isPlaying)
            {
                Weapon plasmaSword = ScriptableObject.CreateInstance<Weapon>();
                //plasmaSword.Init("CheaterSword", "This is cheat activated", Resources.Load<Sprite>("Plasma Sword"), EquipmentSlot.MainHand);
                GameObject.Find("GameManager").GetComponent<Inventory>().Add(plasmaSword);
            }
            else
            {
                Debug.LogError("Not in play mode.");
            }
        }

        [MenuItem("Roguelands Magicite War/Cheats/AddBlaster")]
        public static void AddBlaster()
        {
            if (Application.isPlaying)
            {
                Weapon plasmaSword = ScriptableObject.CreateInstance<Weapon>();
                //plasmaSword.Init("CheaterBlaster", "This is cheat activated", Resources.Load<Sprite>("Blaster"), EquipmentSlot.OffHand);
                GameObject.Find("GameManager").GetComponent<Inventory>().Add(plasmaSword);
            }
            else
            {
                Debug.LogError("Not in play mode.");
            }
        }

        [MenuItem("Roguelands Magicite War/Cheats/Drop Item")]
        public static void DropWeapon()
        {
            if (Application.isPlaying)
            {
                GameObject.Instantiate(Resources.Load<GameObject>("WeaponDrop"), Vector3.zero, Quaternion.identity);
            }
            else
            {
                Debug.LogError("Not in play mode.");
            }
        }

#endif
    }
}