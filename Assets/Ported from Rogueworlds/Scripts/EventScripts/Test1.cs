﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Test1 : MonoBehaviour
{
    private UnityAction someListener;

    private void Awake()
    {
        someListener = new UnityAction(SomeFunction);
    }

    void SomeFunction()
    {
        Debug.Log("EVENT TRIGGERED!!!!  OHAI!");
    }

    private void OnEnable()
    {
        EventManager.StartListening("test",someListener);
    }

    private void OnDisable()
    {
        EventManager.StopListening("test",someListener);
    }
}
