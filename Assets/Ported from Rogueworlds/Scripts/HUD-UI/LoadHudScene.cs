﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadHudScene : MonoBehaviour {

	// Use this for initialization
    private void Start () {
        SceneManager.LoadScene("HUD", LoadSceneMode.Additive);
	}
}
