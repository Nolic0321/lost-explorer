﻿using Photon.Pun;
using System;
using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    public delegate void HealthChangedEventHandler(int newValue);

    public interface IHealthChangedEvent
    {
        event HealthChangedEventHandler HealthChanged;
    }

    public class CharacterStats : MonoBehaviourPunCallbacks, IPunObservable, IHealthChangedEvent
    {
        #region Public Variables

        [Header("Optional: ")]
        public Stat Damage;

        public int MaxHealth;

        public event HealthChangedEventHandler HealthChanged;

        public int CurrentHealth
        {
            get { return _currentHealth; }
            set
            {
                _currentHealth = value;
                if (HealthChanged != null)
                {
                    Debug.Log("CharacterStats: Invoking HealthChanged");
                    HealthChanged.Invoke(_currentHealth);
                }
            }
        }

        #endregion Public Variables

        #region Private Variables

        [SerializeField]
        protected int _currentHealth;

        private bool _isDead = false;

        #endregion Private Variables

        public void Attack(CharacterStats stats)
        {
            stats.TakeDamage(Damage.GetValue());
        }

        public virtual void Die()
        {
            _isDead = true;
            AmDead(_isDead);
        }

        public virtual void Setup()
        {
            //Stub method for now
        }

        public virtual void TakeDamage(int dmg)
        {
            if (_isDead)
            {
                Debug.LogErrorFormat("{0} should be dead but isn't", name);
                return;
            }

            dmg = Mathf.Clamp(dmg, 0, int.MaxValue);
            CurrentHealth = Mathf.Clamp(CurrentHealth - dmg, 0, int.MaxValue);

            if (CurrentHealth <= 0)
            {
                Die();
            }
        }

        protected virtual void VerifyServerHealthValue(int fromServer)
        {
            //Stubbed to be overwritten by child classes
        }

        private void AmDead(bool dead)
        {
            if (!dead)
                Debug.LogErrorFormat("{0} is calling server-side dead but isDead = {1}", name, dead);
            _isDead = dead;
            PhotonNetwork.Destroy(gameObject);
        }

        #region Monobehaviour Callbacks

        protected virtual void Start()
        {
            CurrentHealth = MaxHealth;
        }

        #endregion Monobehaviour Callbacks

        #region Photon IObservable Implementation

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(CurrentHealth);
            }
            else
            {
                CurrentHealth = (int)stream.ReceiveNext();
            }
        }

        #endregion Photon IObservable Implementation
    }
}