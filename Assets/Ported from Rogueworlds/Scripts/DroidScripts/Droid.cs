﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PandaRooStudios.LostExplorer
{
    public class Droid : NetworkBehaviour
    {
        public Player player;
        private Stat expStat;
        private Stat oreMiningStat;
        private Stat plantGatheringStat;
        private Stat woodCuttingStat;

        public void Initialize()
        {
            woodCuttingStat = new Stat();
            plantGatheringStat = new Stat();
            oreMiningStat = new Stat();
            expStat = new Stat();
        }

        protected virtual void ActivateAbility()
        {
            Debug.Log(name + " activated ability!");
        }

        protected virtual void Harvest(Vector2 pos)
        {
            Debug.LogFormat("{0} is going to {1} to harvest", name, pos);
        }
    }
}