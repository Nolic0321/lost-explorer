﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Experimental.PlayerLoop;

public class DroidMovement : NetworkBehaviour
{
    [SerializeField] [Range(3, 10)] private int thinkingTime = 5;
    [SerializeField] private int heightAbovePlayer = 3;
    internal Transform player;
    private Vector2 moveTo;
    private float yMax = 11;
    private float yMin = -12;
    private float xMax = 12.5f;
    private float xMin = -12.5f;

    public IEnumerator MakeDecision()
    {
        while (true)
        {
            float waitTime = Random.Range(3, thinkingTime);
            Coroutine move = StartCoroutine(MoveToNewSpot(new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax))));
            yield return new WaitForSecondsRealtime(waitTime);
            StopCoroutine(move);
        }

    }

    IEnumerator MoveToNewSpot(Vector2 droidPos)
    {
        while (true)
        {
            Vector2 newPos = new Vector2(player.position.x + droidPos.x, player.position.y + heightAbovePlayer + droidPos.y);
            transform.position = Vector2.Lerp(transform.position, newPos, 2f * Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
        }
    }
}
