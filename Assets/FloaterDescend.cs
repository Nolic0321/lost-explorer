﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloaterDescend : MonoBehaviour
{
    public bool descend = false;

    // Start is called before the first frame update
    private void Start()
    {
        if (descend)
        {
            GetComponent<Animator>().SetBool("descend", true);
        }
    }

    // Update is called once per frame
    private void Update()
    {
    }
}