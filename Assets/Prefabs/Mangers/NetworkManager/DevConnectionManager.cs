﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class DevConnectionManager : MonoBehaviourPunCallbacks
    {
        [SerializeField] private bool ignorePlayerJoined = false;
        [SerializeField] private byte maxPlayerInDev = 4;
        [SerializeField] private bool offlineMode = false;
        [SerializeField] private bool searchForExistingRooms = false;

        public delegate void PlayerJoinedRoom();

        public event PlayerJoinedRoom OnPlayerJoinedRoom;

        #region MonoBehaviour Callbacks

        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            PhotonNetwork.GameVersion = "1";
            PhotonNetwork.NickName = "Editor";
            PhotonNetwork.OfflineMode = offlineMode;
            if (!offlineMode)
            {
                PhotonNetwork.ConnectUsingSettings();
                Debug.Log("DevPhoton: Connecting to master server...");
            }
        }

        #endregion MonoBehaviour Callbacks

        #region MonoBehaviourPunCallbacks Callbacks

        public override void OnConnectedToMaster()
        {
            Debug.Log("DevPhoton.OnConnectedToMaster(): Called by PUN");
            if (searchForExistingRooms && !offlineMode)
            {
                Debug.Log("Trying to join a random room");
                PhotonNetwork.JoinRandomRoom();
            }
            else
                CreateRoom();
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("DevPhoton.OnJoinedRoom: Called by PUN");
            if (OnPlayerJoinedRoom != null && !ignorePlayerJoined)
            {
                OnPlayerJoinedRoom.Invoke();
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.LogWarning("DevPhoton.OnJoinRoomFailed(): " + message);
            CreateRoom();
        }

        #endregion MonoBehaviourPunCallbacks Callbacks

        #region Private Methods

        private void CreateRoom()
        {
            Debug.Log("DevPhoton.CreateRoom(): Creating Room");
            PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayerInDev });
        }

        #endregion Private Methods
    }
}