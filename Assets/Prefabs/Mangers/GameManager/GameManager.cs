﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PandaRooStudios.LostExplorer
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        #region Public Variables

        public GameObject playerPrefab;
        public Transform playerSpawnPoint;

        public delegate void PlayerSpawned(Player player);

        public event PlayerSpawned PlayerSpawnedCallback;

        #endregion Public Variables

        #region Private Variables

        [SerializeField] private bool autoConnectPlayer = false;

        //TODO: This should be an interface at some point
        private DevConnectionManager connectionManager;

        #endregion Private Variables

        #region Public Properites

        public bool AutoConnectPlayer
        {
            get
            {
                return autoConnectPlayer;
            }

            set
            {
                autoConnectPlayer = value;
            }
        }

        #endregion Public Properites

        #region Photon Callbacks

        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
        {
            Debug.LogFormat("OnPlayerEnteredRoom() {0}", newPlayer.NickName);

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient);

                // LoadDevScene();
            }
        }

        public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
        {
            Debug.LogFormat("OnPlayerLeftRoom() {0}", otherPlayer.NickName);

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlyaerLeftRoom() IsMasterClient {0}", PhotonNetwork.IsMasterClient);
            }
        }

        #endregion Photon Callbacks

        #region Monobehavior Callbacks

        private void Awake()
        {
            connectionManager = GameObject.FindObjectOfType<DevConnectionManager>();
            connectionManager.OnPlayerJoinedRoom += ConnectPlayer;
        }

        private void OnDestroy()
        {
            connectionManager.OnPlayerJoinedRoom -= ConnectPlayer;
        }

        private void Start()
        {
            if (AutoConnectPlayer)
                ConnectPlayer();
        }

        #endregion Monobehavior Callbacks

        #region Private Methods

        private void LoadScene(string sceneName)
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogError("PhotonNetwork: Trying to Load a level but we are not master Client.");
            }
            Debug.Log("PhotonNetwork: Loading Level " + sceneName);
            PhotonNetwork.LoadLevel(sceneName);
        }

        #endregion Private Methods

        #region Public Methods

        public void ConnectPlayer()
        {
            if (playerPrefab == null)
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference", this);
            }
            else
            {
                //if (MPPlayerManager.localPlayerInstance == null)
                var playerGO = PhotonNetwork.Instantiate(playerPrefab.name, playerSpawnPoint.position, Quaternion.identity, 0);
                if (PlayerSpawnedCallback != null)
                {
                    PlayerSpawnedCallback.Invoke(playerGO.GetComponent<Player>());
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().enabled = true;
                }
            }
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        #endregion Public Methods
    }
}