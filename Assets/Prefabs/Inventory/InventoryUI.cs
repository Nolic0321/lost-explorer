﻿using PandaRooStudios.LostExplorer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class InventoryUI : MonoBehaviour
    {
        public Transform PackPanel;
        public Transform EquipmentPanel;
        private Inventory _inventory;
        private EquipmentManager _equipManager;
        private Slot[] _inventorySlots;

        private Slot[] _equipmentSlots;

        // Use this for initialization
        private void Awake()
        {
            _inventory = GetComponent<Inventory>();
            _inventory.OnItemChangedCallback += UpdateUi;
            _equipManager = GameObject.Find("GameManager").GetComponent<EquipmentManager>();
            _equipManager.OnEquipmentChangedCallback += UpdateEquipmentUi;
            _inventorySlots = PackPanel.GetComponentsInChildren<Slot>();
            _equipmentSlots = EquipmentPanel.GetComponentsInChildren<Slot>();
            for (int i = 0; i < _inventorySlots.Length; i++)
            {
                _inventorySlots[i].Id = i;
            }
        }

        private void OnDestroy()
        {
            _inventory.OnItemChangedCallback -= UpdateUi;
            _equipManager.OnEquipmentChangedCallback -= UpdateEquipmentUi;
        }

        private void UpdateEquipmentUi(Item old, Item bar)
        {
            for (int i = 0; i < _equipmentSlots.Length; i++)
            {
                int equipTypeIndex = (int)_equipmentSlots[i].EquipmentType;
                //and there is Equipment in the EquipmentManager for that slot
                if (_equipManager.CurrentEquipment[equipTypeIndex] != null)
                    //Add the Equipment to this slot
                    _equipmentSlots[i].AddItem(_equipManager.CurrentEquipment[equipTypeIndex]);
                else
                    //Otherwise clear the slot
                    _equipmentSlots[i].ClearSlot();
            }
        }

        public void UpdateUi()
        {
            for (int i = 0; i < _inventorySlots.Length; i++)
            {
                //If there is inventory for this slot
                if (_inventory.Items[i] != null)
                    //Add it to the slot
                    _inventorySlots[i].AddItem(_inventory.Items[i]);
                else
                    //Clear the slot
                    _inventorySlots[i].ClearSlot();
            }
        }
    }
}