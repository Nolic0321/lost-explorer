﻿using UnityEngine;
using UnityEngine.Assertions;

namespace PandaRooStudios.LostExplorer
{
    public class EquipmentManager : MonoBehaviour
    {
        protected EquipmentManager()
        {
        }

        public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);

        public event OnEquipmentChanged OnEquipmentChangedCallback;

        public Equipment[] CurrentEquipment;

        private void Start()
        {
            CurrentEquipment = new Equipment[System.Enum.GetNames(typeof(EquipmentSlot)).Length];
        }

        public void Equip(Equipment item)
        {
            //TODO: Need to determine equipmentSlot swapping
            int slotIndex = (int)item.EquipSlot;

            CurrentEquipment[slotIndex] = item;
            InvokeEquipmentChangedCallback(item, slotIndex);
        }

        private void InvokeEquipmentChangedCallback(Equipment item, int slotIndex)
        {
            if (OnEquipmentChangedCallback != null)//TODO: Call PlayerStats to update stats
                OnEquipmentChangedCallback.Invoke(item, CurrentEquipment[slotIndex]);
        }

        public void UnEquip(int slotIndex)
        {
            Assert.IsNotNull(CurrentEquipment[slotIndex]);
            CurrentEquipment[slotIndex] = null;
            InvokeEquipmentChangedCallback(null, slotIndex);
        }

        public void UnEquip(Equipment equipment)
        {
            UnEquip((int)equipment.EquipSlot);
        }
    }
}