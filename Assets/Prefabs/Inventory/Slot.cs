﻿using PandaRooStudios.LostExplorer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PandaRooStudios.LostExplorer
{
    public class Slot : MonoBehaviour, IDropHandler
    {
        public int Id;
        public Item Item;
        public Image Icon;
        public bool IsEquipmentSlot = false;
        public EquipmentSlot EquipmentType;
        private Inventory _inventory;
        private EquipmentManager _equipManager;

        private void Start()
        {
            _inventory = GameObject.Find("GameManager").GetComponent<Inventory>();
            _equipManager = GameObject.Find("GameManager").GetComponent<EquipmentManager>();
        }

        public void OnDrop(PointerEventData eventData)
        {
            PandaRooStudios.LostExplorer.Item draggedItem = ItemDragHandler.ItemBeingDragged;
            Slot startingSlot = ItemDragHandler.StartingSlot;
            //Equipment Slot
            if (IsEquipmentSlot && draggedItem is Equipment)
            {
                bool existingItem = false;
                Equipment equip = (Equipment)draggedItem;

                //If the equipment actually fits this slot
                if (equip.EquipSlot == EquipmentType)
                {
                    //If there is already equipment in the slot we need to swap it out first
                    if (_equipManager.CurrentEquipment[(int)equip.EquipSlot] != null)
                    {
                        _inventory.Remove(startingSlot.Id);
                        _inventory.Add(_equipManager.CurrentEquipment[(int)equip.EquipSlot], startingSlot.Id);
                        existingItem = true;
                    }
                    //Equip the new equipment
                    _equipManager.Equip(equip);

                    //If there wasn't existing equipment to swap out we need to remove the draggedItem from inventory
                    if (!existingItem)
                        _inventory.Remove(startingSlot.Id);
                }
            }

            //Inventory Slot
            if (!IsEquipmentSlot)
            {
                //If we're coming from the Equipment section
                if (startingSlot.IsEquipmentSlot)
                {
                    //If there is already an item in this slot
                    if (Item != null)
                    {
                        //Attempt to add to "open" inventory
                        _inventory.Add(Item);
                        return; //All done!
                    }

                    UnityEngine.Assertions.Assert.IsTrue(draggedItem is Equipment);
                    //Unequp the item
                    _equipManager.UnEquip((Equipment)draggedItem);
                    //Add it to our inventory
                    _inventory.Add(draggedItem, Id);
                }
                //Swap the items
                else
                {
                    //this should be "ok" with null values as we just switch the empty (null) item with the actual item
                    _inventory.SwapById(Id, startingSlot.Id);
                }
            }
        }

        //Old Code
        //public void OnDrop(PointerEventData eventData)
        //{
        //    Item draggedItem = ItemDragHandler.itemBeingDragged;
        //    Slot startingSlot = ItemDragHandler.startingSlot;

        //    //Equip -> Inv
        //    if (startingSlot.isEquipmentSlot && !isEquipmentSlot)
        //        //Check if there is space in Inv
        //        if (inventory.Add(draggedItem, id)) {
        //            //If there is space then unequip
        //            equipManager.UnEquip((Equipment)draggedItem);

        //        }

        //    //Inv -> Equip
        //    if (!startingSlot.isEquipmentSlot && isEquipmentSlot && draggedItem is Equipment)
        //    {
        //        //Remove draggedItem from inventory
        //        inventory.Remove(startingSlot.id);
        //        //Equip the new item
        //        equipManager.Equip((Equipment)draggedItem);
        //        //If there is an item in this slot
        //        if (item != null)
        //            //Add item to startingSlot
        //            inventory.Add(item, startingSlot.id);
        //    }

        //    //Inv -> Inv
        //    if (!startingSlot.isEquipmentSlot && !isEquipmentSlot)
        //    {
        //        //If there's an existing item
        //        if (item != null)
        //        {
        //            //Swap them
        //            inventory.SwapById(startingSlot.id, id);
        //        }
        //        //If there isn't an existing item
        //        else
        //        {
        //            //Just add the draggedItem
        //            inventory.Add(draggedItem, id);
        //            //Remove it from the old spot
        //            inventory.Remove(startingSlot.id);
        //        }

        //    }

        //    //Equip -> Equip
        //    if(startingSlot.isEquipmentSlot && isEquipmentSlot && draggedItem is Equipment)
        //    {
        //    }
        //}

        public void AddItem(Item newItem)
        {
            if (newItem == null)
                return;

            Item = newItem;

            Icon.sprite = Item.Icon;
            Icon.enabled = true;
        }

        public void ClearSlot()
        {
            Item = null;

            Icon.sprite = null;
            Icon.enabled = false;
        }

        //public List<int> ReturnSelectedElements()
        //{
        //    List<int> selectedElements = new List<int>();
        //    for (int i = 0; i < System.Enum.GetValues(typeof(EquipmentSlot)).Length; i++)
        //    {
        //        int layer = 1 << i;
        //        if (((int)typesAccepted & layer) != 0)
        //        {
        //            selectedElements.Add(i);
        //        }
        //    }

        //    return selectedElements;

        //}
    }
}