﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PandaRooStudios.LostExplorer
{
    public class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
    {
        public static Item ItemBeingDragged;
        public static Slot StartingSlot;

        public void OnBeginDrag(PointerEventData eventData)
        {
            ItemBeingDragged = GetComponentInParent<Slot>().Item;
            StartingSlot = GetComponentInParent<Slot>();
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = Input.mousePosition;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Slot currentSlot = GetComponentInParent<Slot>();
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            transform.localPosition = Vector3.zero;
        }
    }
}