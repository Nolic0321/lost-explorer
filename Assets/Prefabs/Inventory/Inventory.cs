﻿using PandaRooStudios.LostExplorer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace PandaRooStudios.LostExplorer
{
    public class Inventory : MonoBehaviour
    {
        #region Public Variables

        public Item[] Items;

        [HideInInspector] public int Space = 30;

        #endregion Public Variables

        #region Events

        public OnItemChanged OnItemChangedCallback;

        public delegate void OnItemChanged();

        #endregion Events

        #region Public Methods

        public bool Add(Item item, int id)
        {
            if (Items[id] != null)
                return false;

            Items[id] = item;
            InvokeItemChangedCallback();
            return true;
        }

        public bool Add(Item item)
        {
            for (int i = 0; i < Items.Length; i++)
            {
                if (Add(item, i))
                    return true;
            }
            return false;
        }

        public void Clear()
        {
            for (int i = 0; i < Items.Length; i++)
            {
                Items[i] = null;
            }
        }

        public void LoadInventory()
        {
            Items = new Item[Space];
        }

        public void Remove(Item item)
        {
            for (int i = 0; i < Items.Length; i++)
            {
                if (Items[i] == item)
                    Items[i] = null;
            }

            InvokeItemChangedCallback();
        }

        public void SwapById(int slot1, int slot2)
        {
            Item temp = Items[slot1];
            Items[slot1] = Items[slot2];
            Items[slot2] = temp;
            InvokeItemChangedCallback();
        }

        #endregion Public Methods

        #region Private Methods

        internal void Remove(int id)
        {
            UnityEngine.Assertions.Assert.IsNotNull(Items[id]);
            Items[id] = null;

            InvokeItemChangedCallback();
        }

        private void InvokeItemChangedCallback()
        {
            if (OnItemChangedCallback != null)
                OnItemChangedCallback.Invoke();
        }

        #endregion Private Methods
    }
}