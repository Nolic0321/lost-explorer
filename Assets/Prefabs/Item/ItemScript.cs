﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class ItemScript : MonoBehaviour
    {
        #region Public Variables
        public Item itemSO;
        #endregion

        #region Private Variables
        private SpriteRenderer SpriteRenderer;
        #endregion

        #region Monobehaviour Callbacks
        // Start is called before the first frame update
        public virtual void Start()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
            SpriteRenderer.sprite = itemSO.Icon;
        }

        // Update is called once per frame
        private void Update()
        {
        }
        #endregion

    }
}