﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class EquipmentScript : ItemScript
    {
        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();
        }

        public virtual void Equip(Player player)
        {
            throw new System.NotImplementedException("EquipmentScript.Equip() called but shouldn't have been; " + name + " needs to have child class override created");
        }
    }
}