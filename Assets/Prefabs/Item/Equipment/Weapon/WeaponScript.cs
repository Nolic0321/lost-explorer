﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class WeaponScript : EquipmentScript
    {
        #region Private Variables
        protected Stat baseDamage;
        #endregion

        public virtual void Attack()
        {
            Debug.LogWarningFormat("Weaponscript: {0}'s base Attack() method is being called; might need to override this method in child class", name);
        }
    }
}