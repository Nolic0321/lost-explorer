﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class MeleeScript : WeaponScript
    {
        #region Private Variables
        private Collider2D weaponHitBox;
        private Player player;
        #endregion

        #region Monobehaviour Callbacks
        private void Awake()
        {
            weaponHitBox = GetComponent<Collider2D>();
            weaponHitBox.enabled = false;
        }

        public override void Start()
        {
            base.Start();
            baseDamage = ((Weapon)itemSO).Damage;
        }

        private void OnDisable()
        {
            player.playerAttacked -= Attack;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Debug.Log("MeleeScript: Triggered with " + collision.gameObject.name);
            var collisionStats = collision.gameObject.GetComponent<CharacterStats>();
            if (collisionStats != null) {
                weaponHitBox.enabled = false;
                collisionStats.TakeDamage(baseDamage.GetValue());
            }
        }
        #endregion

        public override void Equip(Player player)
        {
            this.player = player;
            this.player.playerAttacked += Attack;
        }
        public override void Attack()
        {
            weaponHitBox.enabled = true;
        }


    }
}