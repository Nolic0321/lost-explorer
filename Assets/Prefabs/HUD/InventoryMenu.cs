﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMenu : MonoBehaviour, IMenu {
    [SerializeField] private GameObject InventoryPanel;

    public void ToggleMenu(){
        InventoryPanel.SetActive(!InventoryPanel.activeSelf);
    }

    public bool IsActive(){
        return InventoryPanel.activeSelf;
    }
}
