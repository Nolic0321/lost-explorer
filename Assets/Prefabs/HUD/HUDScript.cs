﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PandaRooStudios.LostExplorer
{
    public class HUDScript : MonoBehaviour
    {
        #region Private Variables

        [SerializeField] private Slider _hpSlider;
        [SerializeField] private TextMeshProUGUI _hpText;
        [SerializeField] private Slider _stamSlider;
        [SerializeField] private TextMeshProUGUI _stamText;

        private Player playerStats;

        #endregion Private Variables

        #region MonoBehaviour Callbacks

        private void Awake()
        {
            FindObjectOfType<GameManager>().PlayerSpawnedCallback += Init;
        }

        private void OnDestroy()
        {
            playerStats.HealthChanged -= UpdateHealth;
            FindObjectOfType<GameManager>().PlayerSpawnedCallback -= Init;
        }

        // Use this for initialization
        private void Start()
        {
            GameObject.DontDestroyOnLoad(gameObject);
        }

        #endregion MonoBehaviour Callbacks

        #region Public Methods

        public void Init(Player stats)
        {
            stats.HealthChanged += UpdateHealth;
            UpdateStat(stats.CurrentHealth, stats.MaxHealth, _hpSlider, _hpText);
        }

        public void UpdateHealth(int currentHealth)
        {
            _hpSlider.value = currentHealth;
        }

        public void UpdateStamina()
        {
            //Stub
        }

        #endregion Public Methods

        #region Private Methods

        private void UpdateStat(int current, int max, Slider slider, TextMeshProUGUI text)
        {
            slider.maxValue = max;
            slider.value = current;
            text.text = string.Format("{0}/{1}", current, max);
        }

        #endregion Private Methods
    }
}