﻿using Photon.Pun;
using System.Collections;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    public class PlayerInput : MonoBehaviourPun
    {
        [SerializeField] private float _jumpMultiplier = 12f;
        [SerializeField] private float _fallMultiplier = 2.5f;
        [SerializeField] private float _lowJumpMultiplier = 2f;
        [SerializeField] private float _playerSpeed = 3;
        [SerializeField] private Transform _playerModel;
        [SerializeField] private PlayerAttack _weaponBox;

        [SerializeField] private Animator _anim;

        private int attackHash = Animator.StringToHash("attack");
        private int isWalkingHash = Animator.StringToHash("isWalking");
        private const float PlayerModelYRotationDegree = 180;
        private UNetChat playerChat;
        private PauseMenu pauseMenu;
        private InventoryMenu inventoryMenu;

        private float _horizontal;
        private bool _isJumping = false;
        private bool inputEnabled = true;
        private Rigidbody2D _rb;
        private Animator _netAnim;

        private float _playerRot;

        // Use this for initialization
        private void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
        }

        public void Init(UNetChat chat, PauseMenu pause, InventoryMenu inventory)
        {
            playerChat = chat;
            playerChat.chatSelectedCallback += ChatSelectedResponse;

            pauseMenu = pause;
            inventoryMenu = inventory;
        }

        private void OnDestroy()
        {
            playerChat.chatSelectedCallback -= ChatSelectedResponse;
        }

        // Update is called once per frame
        private void Update()
        {
            if (!pauseMenu.IsActive() && !inventoryMenu.IsActive())
            {
                _horizontal = Input.GetAxisRaw("Horizontal") * _playerSpeed;
                FlipSprite();

                if (Input.GetButtonDown("Jump"))
                    _isJumping = true;

                if (Input.GetButtonDown("Fire1") && _weaponBox)
                {
                    _anim.SetTrigger(attackHash);
                    _weaponBox.Attack();
                    CmdAttackAnim();
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                pauseMenu.ToggleMenu();
            }

            if (Input.GetKeyDown(KeyCode.I))
            {
                inventoryMenu.ToggleMenu();
            }
        }

        private void CmdAttackAnim()
        {
            RpcSetTrigger();
        }

        private void RpcSetTrigger()
        {
            if (!photonView.IsMine)
                _anim.SetTrigger(attackHash);
        }

        private void ServerFlippedSprite(float rot)
        {
            _playerModel.rotation = Quaternion.Euler(0, rot, 0);
        }

        private void FlipSprite()
        {
            if (Mathf.Abs(MouseAngle()) > 90)
                _playerModel.SetPositionAndRotation(_playerModel.transform.position, Quaternion.Euler(0, PlayerModelYRotationDegree, 0));
            else if (Mathf.Abs(MouseAngle()) < 90)
                _playerModel.SetPositionAndRotation(_playerModel.transform.position, Quaternion.Euler(0, 0, 0));

            //Make a Server-side Flip
            CmdFlipSprite(_playerModel.rotation.eulerAngles.y);
        }

        private void CmdFlipSprite(float rot)
        {
            _playerRot = rot;
        }

        private void FixedUpdate()
        {
            if (_isJumping)
            {
                _rb.AddForce(Vector2.up * _jumpMultiplier, ForceMode2D.Impulse);
                _isJumping = false;
            }
            if (_rb.velocity.y < 0)
                _rb.velocity += Vector2.up * Physics2D.gravity.y * (_fallMultiplier - 1) * Time.deltaTime;
            else if (_rb.velocity.y > 0 && !Input.GetButton("Jump"))
                _rb.velocity += Vector2.up * Physics2D.gravity.y * (_lowJumpMultiplier - 1) * Time.deltaTime;

            _rb.AddForce(new Vector2(_horizontal, 0), ForceMode2D.Impulse);
            _anim.SetBool(isWalkingHash, _rb.velocity.x != 0);
        }

        private float MouseAngle()
        {
            var pos = Camera.main.WorldToScreenPoint(transform.position);
            var dir = Input.mousePosition - pos;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            return angle;
        }

        public void ChatSelectedResponse(bool chatSelected)
        {
            enabled = !chatSelected;
        }
    }
}