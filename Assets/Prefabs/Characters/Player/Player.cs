﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandaRooStudios.LostExplorer
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Player : CharacterStats
    {
        #region Private Variables

        private Animator Animator;
        [SerializeField] private float fallMultiplier;
        private float horizontal;
        [SerializeField] private float jumpMultiplier;
        [SerializeField] private float lowJumpMultiplier;
        [SerializeField] private GameObject mainHandSlot;
        private int MELEE_ATTACK_HASH;
        [SerializeField] private float movementSpeed;
        private Rigidbody2D rb;

        #endregion Private Variables

        #region Public Variables

        public Equipment debugEquipment;

        public delegate void PlayerAttacking();

        public event PlayerAttacking playerAttacked;

        public delegate void PickupItem(Item item);
        public event PickupItem pickedUpItem;

        #endregion Public Variables

        #region CharacterStats Abstract Implementation

        public override void Die()
        {
            Debug.LogFormat("Player: {0} is dying.", name);
        }

        #endregion CharacterStats Abstract Implementation

        #region Monobehaviour Callbacks

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            Animator = GetComponent<Animator>();
            MELEE_ATTACK_HASH = Animator.StringToHash("Melee Attack");
            rb = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        private void Update()
        {
            //Only listen to input if it's our player
            if (!photonView.IsMine)
                return;

            if (Input.GetButtonDown("Fire1"))
            {
                if (playerAttacked != null)
                {
                    playerAttacked.Invoke();
                    Animator.SetTrigger(MELEE_ATTACK_HASH);
                }
            }

            if (Input.GetKeyDown(KeyCode.E) && Debug.isDebugBuild)
            {
                Equip(debugEquipment);
            }
            //Check horizontal movement
            rb.AddForce(Input.GetAxisRaw("Horizontal") * movementSpeed * Vector2.right, ForceMode2D.Impulse);

            if (Input.GetButtonDown("Jump"))
            {
                var jumpValue = Vector3.up * jumpMultiplier;
                rb.AddForce(jumpValue, ForceMode2D.Impulse);
            }
            //Fall Code
            if (rb.velocity.y < 0)
                rb.AddForce(Vector2.up * Physics.gravity.y * (fallMultiplier - 1), ForceMode2D.Force);
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
                rb.AddForce(Vector2.up * Physics.gravity.y * (lowJumpMultiplier - 1), ForceMode2D.Force);
        }

        #endregion Monobehaviour Callbacks

        #region Public Methods

        public void Equip(Equipment equipment)
        {
            {
                if (equipment is Weapon)
                {
                    GameObject go = Instantiate(equipment.EquipablePrefab, mainHandSlot.transform);
                    go.GetComponent<EquipmentScript>().Equip(this);
                }
            }
        }

        #endregion Public Methods

        private IEnumerator PulseSprites()
        {
            SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
            while (true)
            {
                SetSpriteAlpha(0);
                yield return new WaitForFixedUpdate();
                yield return new WaitForSeconds(.1f);
                SetSpriteAlpha(1);
                yield return new WaitForFixedUpdate();
                yield return new WaitForSeconds(.1f);
            }
        }

        private void SetSpriteAlpha(int alpha)
        {
            SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].color = new Color(1, 1, 1, alpha);
            }
        }
    }
}