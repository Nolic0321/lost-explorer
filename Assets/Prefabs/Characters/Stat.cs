﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    [SerializeField]
    public int BaseValue;

    private List<int> _modifiers = new List<int>();

    public Stat()
    {
        BaseValue = 0;
    }

    public Stat(int baseValue)
    {
        BaseValue = baseValue;
    }

    public int GetValue()
    {
        int finalValue = BaseValue;
        _modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }

    public void AddModifier(int mod)
    {
        if (mod != 0)
        {
            _modifiers.Add(mod);
        }
    }

    public void RemoveModifier(int mod)
    {
        if (mod != 0)
        {
            _modifiers.Remove(mod);
        }
    }
}