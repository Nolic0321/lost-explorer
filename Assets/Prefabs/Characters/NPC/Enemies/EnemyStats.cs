﻿using Photon.Pun;
using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace PandaRooStudios.LostExplorer
{
    public class EnemyStats : CharacterStats
    {
        #region Private Variables

        [SerializeField] private GameObject _dropItemPrefab;

        #endregion Private Variables

        #region Public Variables

        [HideInInspector] public PrefabManager prefabManager;

        #endregion Public Variables

        #region Monobehaviour Callbacks

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Debug.Log("EnemyStats: Colliding with " + collision.gameObject.name);
            var colliderStats = collision.gameObject.GetComponent<CharacterStats>();
            if (colliderStats != null)
                Attack(collision.gameObject.GetComponent<CharacterStats>());
        }

        #endregion Monobehaviour Callbacks

        #region Public Methods

        public override void Die()
        {
            RpcDropItem();
            prefabManager.SpawnEnemy();
        }

        #endregion Public Methods

        #region Private Methods

        private void RpcDropItem()
        {
            Instantiate(_dropItemPrefab, transform.position, Quaternion.identity);
            base.Die();
        }

        #endregion Private Methods
    }
}