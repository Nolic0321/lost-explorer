﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PandaRooStudios.LostExplorer
{
    public class HealthBar : MonoBehaviour
    {
        #region Private Variables

        private CharacterStats characterStats;
        [SerializeField] private Slider slider;

        #endregion Private Variables

        #region Monobehaviour Callbacks

        // Start is called before the first frame update
        private void Awake()
        {
            characterStats = GetComponentInParent<CharacterStats>();
            slider.maxValue = characterStats.MaxHealth;
            characterStats.HealthChanged += UpdateBar;
        }

        private void OnDisable()
        {
            characterStats.HealthChanged -= UpdateBar;
        }

        #endregion Monobehaviour Callbacks

        #region Private Methods

        private void UpdateBar(int newValue)
        {
            slider.value = newValue;
        }

        #endregion Private Methods
    }
}