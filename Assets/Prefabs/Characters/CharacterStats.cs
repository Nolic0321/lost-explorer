﻿using UnityEngine;

namespace PandaRooStudios
{
    public abstract class CharacterStats : MonoBehaviour
    {
        #region Private Variables

        [SerializeField] private Stat health;
        [SerializeField] private Stat damage;
        [SerializeField] private Stat carryingCapacity;

        #endregion Private Variables

        #region Properites

        public int Health
        {
            get
            {
                return health.GetValue();
            }

            set
            {
                health.AddModifier(value);
            }
        }

        public int Damage
        {
            get
            {
                return damage.GetValue();
            }

            set
            {
                damage.AddModifier(value);
            }
        }

        public int CarryingCapacity
        {
            get
            {
                return carryingCapacity.GetValue();
            }

            set
            {
                carryingCapacity.AddModifier(value);
            }
        }

        #endregion Properites

        #region Delegates

        public delegate void OnDying();

        public OnDying onDying;

        #endregion Delegates

        #region Public Methods

        public void Attack(CharacterStats otherCharacter)
        {
            Debug.LogFormat("{0} is attacking {1}", gameObject.name, otherCharacter.name);
            otherCharacter.TakeDamage(Damage);
        }

        private void TakeDamage(int damage)
        {
            health.AddModifier(-damage);
            if (health.GetValue() <= 0)
            {
                IsDying();
            }
        }

        #endregion Public Methods

        #region Virtual Methods

        public abstract void IsDying();

        #endregion Virtual Methods
    }
}